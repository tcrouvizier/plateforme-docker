= Environnement Docker


== Nginx

https://www.nginx.com/resources/wiki/start/topics/examples/full/

=== Configuration

Ajout des fichiers `index.php` aux index par défaut.

[source, yaml]
.nginx/conf/application.conf
----
server {
    index index.html index.php;
    ....
----

=== logs

Les logs sont accessibles de la manière suivante (access.log et error.log).

[source, bash]
----
$ docker logs docker_web_1
----

== PHP-FPM

=== Configuration

Configuration de Nginx pour transférer les fichiers php vers le serveur FPM.

[source, yaml]
.nginx/conf/application.conf
----
server {
    index index.html index.php;
    ....

    location ~ \.php$ {
        try_files $uri =404;
        fastcgi_split_path_info ^(.+\.php)(/.+)$;
        fastcgi_pass php:9000;
        fastcgi_index index.php;
        include fastcgi_params;
        fastcgi_param SCRIPT_FILENAME $document_root$fastcgi_script_name;
        fastcgi_param PATH_INFO $fastcgi_path_info;
    }
  }
----

== PHP-CLI

Ajout d'un `Dockerfile` pour ajouter facilement des extensions PHP.

== Composer

Ajout d'un `Dockerfile` pour ajouter facilement des extensions PHP (i.e. pour AMQP).

== Ressources

* http://geekyplatypus.com/dockerise-your-php-application-with-nginx-and-php7-fpm/
* http://geekyplatypus.com/making-your-dockerised-php-application-even-better/
